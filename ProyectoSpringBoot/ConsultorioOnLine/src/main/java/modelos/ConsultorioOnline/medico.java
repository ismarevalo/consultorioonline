/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class medico {
    // Atributos
    private int fk_persona;
    private int fk_especialidad;
    
    // Constructor

    public medico(int fk_persona, int fk_especialidad) {
        this.fk_persona = fk_persona;
        this.fk_especialidad = fk_especialidad;
    }

    public medico() {
    }
    
    // Metodos

    public int getFk_persona() {
        return fk_persona;
    }

    public void setFk_persona(int fk_persona) {
        this.fk_persona = fk_persona;
    }

    public int getFk_especialidad() {
        return fk_especialidad;
    }

    public void setFk_especialidad(int fk_especialidad) {
        this.fk_especialidad = fk_especialidad;
    }
    

}
