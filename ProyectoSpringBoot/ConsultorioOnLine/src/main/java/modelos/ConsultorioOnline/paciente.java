/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class paciente {
    // Atributos
    private int fk_persona2;
    private int eps;
    
    // Contructor

    public paciente(int fk_persona2, int eps) {
        this.fk_persona2 = fk_persona2;
        this.eps = eps;
    }

    public paciente() {
    }
    
    // Metodos

    public int getFk_persona2() {
        return fk_persona2;
    }

    public void setFk_persona2(int fk_persona2) {
        this.fk_persona2 = fk_persona2;
    }

    public int getEps() {
        return eps;
    }

    public void setEps(int eps) {
        this.eps = eps;
    }
    

}
