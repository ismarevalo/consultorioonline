/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class citamedica {
    // Atributos
    private int fk_paciente;
    private int fk_medico;
    private float fecha;
        
    // Construtor

    public citamedica(int fk_paciente, int fk_medico, float fecha) {
        this.fk_paciente = fk_paciente;
        this.fk_medico = fk_medico;
        this.fecha = fecha;
    }

    public citamedica() {
    }
    
    // Metodos

    public int getFk_paciente() {
        return fk_paciente;
    }

    public void setFk_paciente(int fk_paciente) {
        this.fk_paciente = fk_paciente;
    }

    public int getFk_medico() {
        return fk_medico;
    }

    public void setFk_medico(int fk_medico) {
        this.fk_medico = fk_medico;
    }

    public float getFecha() {
        return fecha;
    }

    public void setFecha(float fecha) {
        this.fecha = fecha;
    }
          
    
}
