/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class diagnostico {
    // Atributos
    private int fk_cita;
    private String diagnostico;
    private String observaciones;
    private int fk_medicamento;
    
    // Constructor
    public diagnostico(int fk_cita, String diagnostico, String observaciones, int fk_medicamento) {
        this.fk_cita = fk_cita;
        this.diagnostico = diagnostico;
        this.observaciones = observaciones;
        this.fk_medicamento = fk_medicamento;
    }

    public diagnostico() {
    }
    
    // Metodos

    public int getFk_cita() {
        return fk_cita;
    }

    public void setFk_cita(int fk_cita) {
        this.fk_cita = fk_cita;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getFk_medicamento() {
        return fk_medicamento;
    }

    public void setFk_medicamento(int fk_medicamento) {
        this.fk_medicamento = fk_medicamento;
    }
    

}
