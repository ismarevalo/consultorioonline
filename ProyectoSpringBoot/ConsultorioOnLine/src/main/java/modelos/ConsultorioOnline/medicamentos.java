/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class medicamentos {
    // Atributos
    private String descripcion;
    private String presentacion;
    private String laboratorio;
    
    // Constructor

    public medicamentos(String descripcion, String presentacion, String laboratorio) {
        this.descripcion = descripcion;
        this.presentacion = presentacion;
        this.laboratorio = laboratorio;
    }

    public medicamentos() {
    }
    
    // Metodos

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }
    
    

}
