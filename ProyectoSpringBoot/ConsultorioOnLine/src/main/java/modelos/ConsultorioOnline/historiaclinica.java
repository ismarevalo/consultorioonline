/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class historiaclinica {
    // Atributos
    private int fk_paciente2;
    
    // Constructor

    public historiaclinica(int fk_paciente2) {
        this.fk_paciente2 = fk_paciente2;
    }

    public historiaclinica() {
    }
    
    // Metodos

    public int getFk_paciente2() {
        return fk_paciente2;
    }

    public void setFk_paciente2(int fk_paciente2) {
        this.fk_paciente2 = fk_paciente2;
    }
    
    

}
