/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class detallehistoria {
    // Atributos
    private int fk_historia;
    private int fk_diagnostico;
    
    // Contrutor
    public detallehistoria(int fk_historia, int fk_diagnostico) {
        this.fk_historia = fk_historia;
        this.fk_diagnostico = fk_diagnostico;
    }
    public detallehistoria() {
    }
     
    // Metodos

    public int getFk_historia() {
        return fk_historia;
    }

    public void setFk_historia(int fk_historia) {
        this.fk_historia = fk_historia;
    }

    public int getFk_diagnostico() {
        return fk_diagnostico;
    }

    public void setFk_diagnostico(int fk_diagnostico) {
        this.fk_diagnostico = fk_diagnostico;
    }
        

}
