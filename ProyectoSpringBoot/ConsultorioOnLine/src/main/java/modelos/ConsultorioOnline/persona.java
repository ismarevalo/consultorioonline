/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package modelos.ConsultorioOnline;

/**
 * 
 * @author German Hernandez <GHernandez at germanarevalo29@gmail.com>
 */
public class persona {
    // Atributos
    private String apellidos;
    private String nombres;
    private int cedula;
    private String sexo;
    private float fecha_nacimiento;
    private String email;
    private String direccion;
    private String telefono;
    
    // Constructor

    public persona(String apellidos, String nombres, int cedula, String sexo, float fecha_nacimiento, String email, String direccion, String telefono) {
        this.apellidos = apellidos;
        this.nombres = nombres;
        this.cedula = cedula;
        this.sexo = sexo;
        this.fecha_nacimiento = fecha_nacimiento;
        this.email = email;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public persona() {
    }
    
    // Metodos

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public float getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(float fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    

}
