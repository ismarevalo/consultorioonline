package ConsultorioOnLine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultorioOnLineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultorioOnLineApplication.class, args);
	}

}
