![fn.png](./fn.png)
<h1>JAVA proyecto usando modelo MVC y base de datos MySQL</h1>
<h2>Autores : Ismael Arévalo González & Germán Arévalo Hernández</h2>
<h2>Profesora : Yhary Estefania Arias</h2>
<h3>Descripción :</h3>
<h4>Esta es una aplicación códificada en JAVA cuyo propósito es gestionar un consultorio médico, gestionar los médicos, especialidades, historias clinicas, medicamentos y citas con una base de datos MySQL usando la estructura Modelo, Vista, Controlador (MVC).</h4>

![fondo3.jpeg](./fondo3.jpeg)

<h3>Funcionalidades :</h3>
<h4>
- Permite autenticar un usuario<br>
- Una vez validado, en caso de ser administrador puede parametrizar las especialidades, médicos, horarios de atención y medicamentos<br>
- Un médico puede modificar sus datos, sus horarios de atención en ese consultorio y consultar su agenda de citas<br>
- Un paciente puede registrarse, crear, borrar, modificar y consultar citas médicas</h4>
